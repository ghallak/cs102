import string


def shift_char(char, shift):
    if not isinstance(char, str):
        raise TypeError("Variable 'char' must be of type 'str'")
    if not isinstance(shift, int):
        raise TypeError("Variable 'shift' must be of type 'int'")

    if char.isupper():
        s = string.ascii_uppercase
    else:
        s = string.ascii_lowercase

    return s[(s.index(char) + shift) % len(s)]


def encrypt_caesar(plaintext, shift):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON", 3)
    'SBWKRQ'
    >>> encrypt_caesar("python", 3)
    'sbwkrq'
    >>> encrypt_caesar("", 3)
    ''
    """

    ciphertext = ""

    for c in plaintext:
        ciphertext += shift_char(c, shift)

    return ciphertext


def decrypt_caesar(ciphertext, shift):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ", 3)
    'PYTHON'
    >>> decrypt_caesar("sbwkrq", 3)
    'python'
    >>> decrypt_caesar("", 3)
    ''
    """

    plaintext = ""

    for c in ciphertext:
        plaintext += shift_char(c, -shift)

    return plaintext
