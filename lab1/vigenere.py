import string
from caesar import shift_char


def encrypt_vigenere(plaintext, keyword):
    """
    Encrypts plaintext using a Vigenere cipher.

    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """

    ciphertext = ""

    for i in range(len(plaintext)):
        shift = string.ascii_lowercase.index(keyword[i % len(keyword)].lower())
        ciphertext += shift_char(plaintext[i], shift)

    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    Decrypts a ciphertext using a Vigenere cipher.

    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """

    plaintext = ""

    for i in range(len(ciphertext)):
        shift = string.ascii_lowercase.index(keyword[i % len(keyword)].lower())
        plaintext += shift_char(ciphertext[i], -shift)

    return plaintext
