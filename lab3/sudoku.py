import math
import solver
from itertools import product


def display(grid):
    unit = len(grid) // 3

    for i, row in enumerate(grid):
        if i > 0 and i % unit == 0:
            for j in range((unit * 2 + 1) * 3):
                if j == unit * 2 or j == unit * 4 + 2:
                    print('+', end='')
                else:
                    print('-', end='')
            print()
        for j, val in enumerate(row):
            if j > 0 and j % unit == 0:
                print('| ', end='')
            print(val, end=('\n' if j == len(row) - 1 else ' '))


def group(values, n):
    """
    Group the elements of 'values' in a list of lists made of 'n' elements

    >>> group([1,2,3,4], 2)
    [[1, 2], [3, 4]]
    >>> group([1,2,3,4,5,6,7,8,9], 3)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    """
    assert isinstance(values, list), "'values' must be a list"
    assert isinstance(n, int), "'n' must be of type 'int'"
    assert n * n == len(values), "'values' cannot form a sudoku"

    return [values[i:i + n] for i in range(0, len(values), n)]


def read_sudoku(filename):
    """ Read the sudoku from the file 'filename' """
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, int(math.sqrt(len(digits))))
    return grid


def get_row(values, pos):
    """ Возвращает все значения для номера строки, указанной в pos

    >>> get_row([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '2', '.']
    >>> get_row([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (1, 0))
    ['4', '.', '6']
    >>> get_row([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (2, 0))
    ['.', '8', '9']
    """
    return values[pos[0]]


def get_col(values, pos):
    """ Возвращает все значения для номера столбца, указанного в pos

    >>> get_col([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '4', '7']
    >>> get_col([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (0, 1))
    ['2', '.', '8']
    >>> get_col([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (0, 2))
    ['3', '6', '9']
    """
    return [l[pos[1]] for l in values]


def get_box(values, pos):
    """ Возвращает все значения из квадрата, в который попадает позиция pos """
    unit = len(values[0]) // 3
    row_start = pos[0] // unit * unit
    col_start = pos[1] // unit * unit

    ret = []

    for i in range(row_start, row_start + unit):
        for j in range(col_start, col_start + unit):
            ret.append(values[i][j])

    return ret


def find_empty_positions(grid):
    """ Найти первую свободную позицию в пазле

    >>> find_empty_positions([['1', '2', '.'], ['4', '5', '6'],
    ...                       ['7', '8', '9']])
    (0, 2)
    >>> find_empty_positions([['1', '2', '3'], ['4', '.', '6'],
    ...                       ['7', '8', '9']])
    (1, 1)
    >>> find_empty_positions([['1', '2', '3'], ['4', '5', '6'],
    ...                       ['.', '8', '9']])
    (2, 0)
    """
    for i, row in enumerate(grid):
        for j, val in enumerate(row):
            if val == '.':
                return i, j


def find_possible_values(grid, pos):
    """ Вернуть все возможные значения для указанной позиции """
    row_exist = get_row(grid, pos)
    col_exist = get_col(grid, pos)
    blk_exist = get_box(grid, pos)

    digits = set('123456789')

    row = digits - set(row_exist)
    col = digits - set(col_exist)
    blk = digits - set(blk_exist)

    return list(set(row) & set(col) & set(blk))


def check_solution(solution):
    """ Returns True for if 'solution is correct. Otherwise False """
    if not all(len(get_row(solution, (i, 0))) ==
               len(set(get_row(solution, (i, 0)))) for i in range(9)):
        return False
    if not all(len(get_col(solution, (0, i))) ==
               len(set(get_col(solution, (0, i)))) for i in range(9)):
        return False
    if not all(len(get_box(solution, (i, j))) ==
               len(set(get_box(solution, (i, j))))
               for i, j in product(range(0, 9, 3), range(0, 9, 3))):
        return False
    return True


def solve(grid):
    return solver.solve_sudoku(grid)
