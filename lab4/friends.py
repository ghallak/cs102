import requests
import statistics
import access_token

from datetime import datetime


def get_friends(user_id, fields):
    """
    Returns a list of user IDs or detailed information about a user's friends
    """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert isinstance(fields, str), "fields must be string"
    assert user_id > 0, "user_id must be positive integer"

    domain = "https://api.vk.com/method"

    query_params = {
        'domain': domain,
        'access_token': access_token.token,
        'user_id': user_id,
        'fields': fields
    }

    query = """\
            {domain}/friends.get?\
            access_token={access_token}&\
            user_id={user_id}&\
            fields={fields}&v=5.53\
            """.replace(" ", "").format(**query_params)
    response = requests.get(query)

    return response.json()['response']['items']


def filter_friends(friends):
    """ Returns a list of friends who has their year of birth shown """
    filtered = []
    for friend in friends:
        if 'bdate' in friend and len(friend['bdate'].split('.')) == 3:
            filtered.append(friend)

    return filtered


def age_predict(user_id):
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"

    friends = get_friends(user_id, 'bdate')
    friends_with_years = filter_friends(friends)
    birth_years = []
    for friend in friends_with_years:
        birth_years.append(int(friend['bdate'].split('.')[2]))

    mode = statistics.mode(birth_years)

    now = datetime.now()
    year = now.year

    return year - mode


if __name__ == "__main__":
    user_id = 1
    prediction = age_predict(user_id)

    print('Predicted age:', prediction)
