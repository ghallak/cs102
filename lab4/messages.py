import requests
import plotly.plotly as py
import plotly.graph_objs as go
import access_token

from datetime import datetime


def messages_get_history(user_id, offset=0, count=100):
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    assert isinstance(offset, int), "offset must be positive integer"
    assert offset >= 0, "user_id must be positive integer"
    assert count >= 0, "user_id must be positive integer"

    domain = "https://api.vk.com/method"

    query_params = {
        'domain': domain,
        'offset': offset,
        'count': count,
        'access_token': access_token.token,
        'user_id': user_id
    }

    query = """\
            {domain}/messages.getHistory?\
            offset={offset}&\
            count={count}&\
            access_token={access_token}&\
            user_id={user_id}&v=5.53\
            """.replace(" ", "").format(**query_params)
    response = requests.get(query)

    return response.json()['response']['items']


def count_dates_from_messages(messages):
    dates = []
    freqs = []
    for message in messages:
        date = datetime.fromtimestamp(message['date']).strftime("%Y-%m-%d")
        if date not in dates:
            # if date is not in the list, add it, and add a frequency of one
            dates.append(date)
            freqs.append(1)
        else:
            # if date is in the list, update its frequency
            freqs[-1] += 1

    # freq in the last date may not be correct because of messages count limit
    try:
        dates.pop()
        freqs.pop()
    except IndexError:
        pass

    return dates, freqs


def create_plot(dates, freqs):
    data = [go.Scatter(x=dates, y=freqs)]
    py.plot(data)


if __name__ == "__main__":
    user_id = 1
    history = messages_get_history(user_id)
    dates, freqs = count_dates_from_messages(history)
    create_plot(dates, freqs)
