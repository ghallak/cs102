import telebot
import config
import access_token
import datetime
import time

import utils
from page import *


access_token = access_token.token
bot = telebot.TeleBot(access_token)


def send_message(chat_id, msg):
    try:
        bot.send_message(chat_id, msg, parse_mode='HTML')
    except:
        bot.send_message(char_id, 'Расписание не найдено')


@bot.message_handler(commands=['near_lesson'],
                     func=lambda msg: len(msg.text.split()) == 2)
def get_near_lesson(message):
    """near_lesson GROUP_NUMBER"""
    group = message.text.split()[1]

    if not is_group(group):
        bot.send_message(message.chat.id, 'The group number is not valid')
        return

    week = get_week(get_page(group))
    web_page = get_page(group, week)
    current_day = utils.get_today()

    for i in range(len(config.week_days)):
        if current_day == 'monday' and i != 0:
            web_page = get_page(group, week)
        try:
            sched = get_schedule(web_page, current_day)
            resp = ''

            # find the first class in the same day that starts after
            # the current time.
            # if the class cannot be found in the same day, find the nearest
            # class in any of the next days.
            if i > 0:
                resp = utils.format_respond([sched[0]], current_day)
            else:
                for i in range(len(times_lst)):
                    t = times_lst[i]
                    c = time.strptime(t.split('-')[0], "%H:%M")
                    now = datetime.datetime.now()
                    lsn = now.replace(hour=c.tm_hour, minute=c.tm_min)
                    if lsn > now:
                        resp = utils.format_respond([sched[i]], current_day)
                        break

            send_message(message.chat.id, resp)
            break
        except:
            current_day, week = utils.get_next_day(current_day, week)


@bot.message_handler(commands=config.week_days,
                     func=lambda msg: len(msg.text.split()) == 3)
def get_day(message):
    """DAY WEEK_NUMBER GROUP_NUMBER"""
    cmd, week, group = message.text.split()

    # remove the slash. e.g., '/monday' will become 'monday'
    day = cmd[1:]

    web_page = get_page(group)
    sched = get_schedule(web_page, day)
    resp = utils.format_respond(sched)

    send_message(message.chat.id, resp)


@bot.message_handler(commands=['tomorrow'],
                     func=lambda msg: len(msg.text.split()) == 2)
def get_tomorrow(message):
    """tommorow GROUP_NUMBER"""
    group = message.text.split()[1]

    if not is_group(group):
        bot.send_message(message.chat.id, 'The group number is not valid')
        return

    week = get_week(get_page(group))
    tomorrow, week = utils.get_next_day(utils.get_today(), week)

    web_page = get_page(group, week)

    try:
        sched = get_schedule(web_page, tomorrow)
        resp = utils.format_respond(sched, tomorrow)
    except AttributeError:
        pass

    send_message(message.chat.id, resp)


@bot.message_handler(commands=['all'],
                     func=lambda msg: len(msg.text.split()) == 3)
def get_all(message):
    """all WEEK_NUMBER GROUP_NUMBER"""
    _, week, group = message.text.split()

    web_page = get_page(group)
    resp = ''

    for day in config.week_days:
        try:
            sched = get_schedule(web_page, day)
            resp += utils.format_respond(sched, day)
        except AttributeError:
            pass

    send_message(message.chat.id, resp)


@bot.message_handler(commands=['help'])
def show_help(message):
    cmds = ['/near_lesson GROUP_NUMBER',
            '/DAY WEEK_NUMBER GROUP_NUMBER',
            '/tomorrow GROUP_NUMBER',
            '/all WEEK_NUMBER GROUP_NUMBER',
            '/help']
    bot.send_message(message.chat.id, '\n'.join(cmds))


if __name__ == '__main__':
    bot.polling(none_stop=True)
