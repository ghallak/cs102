import requests
import config
from bs4 import BeautifulSoup
from collections import namedtuple


SchedEntry = namedtuple('SchedEntry',
                        ['time', 'address', 'room', 'lesson', 'teacher'])


def is_group(group):
    """Returns True if 'group' has schedule on the website. Otherwise False"""
    web_page = get_page(group)
    no_sched = 0
    for day in config.week_days:
        try:
            get_schedule(web_page, day)
        except AttributeError:
            no_sched += 1

    return no_sched < len(config.week_days)


def get_week(web_page):
    """Returns 1 for even weeks or 2 for odd weeks"""
    soup = BeautifulSoup(web_page, "lxml")

    date = soup.find("h2", attrs={"class": "schedule-week"})
    week = date.find("strong").text

    if week.lower() == 'четная':
        return 1
    else:
        return 2


def get_page(group, week=''):
    if week:
        week = str(week) + '/'
    url = '{domain}/{group}/{week}raspisanie_zanyatiy_{group}.htm'.format(
        domain=config.domain,
        week=week,
        group=group)
    response = requests.get(url)
    web_page = response.text

    return web_page


def get_schedule(web_page, day):
    soup = BeautifulSoup(web_page, "lxml")

    day_num = config.week_days.index(day)

    # Schedule for 'day'
    schedule_table = soup.find("table", attrs={"id": str(day_num + 1) + "day"})

    # Times for the classes
    times_list = schedule_table.find_all("td", attrs={"class": "time"})
    times_list = [time.span.text for time in times_list]

    # Places for the classes
    locations_list = schedule_table.find_all("td", attrs={"class": "room"})
    adresses_list = [room.span.text for room in locations_list]
    rooms_list = [room.dd.text for room in locations_list]

    # Names of the classes and names of the teachers
    classes_list = schedule_table.find_all("td", attrs={"class": "lesson"})
    teachers_list = [cls.dt.text for cls in classes_list]
    lessons_list = [cls.dd.text for cls in classes_list]

    return [SchedEntry(*entry) for entry in zip(times_list, adresses_list,
                                                rooms_list, lessons_list,
                                                teachers_list)]
