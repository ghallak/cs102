import datetime
import config


def get_today():
    """Returns the name of the current day"""
    return datetime.datetime.now().strftime("%A").lower()


def get_next_day(day, week):
    """Returns the name of the week day after 'day' and the current week"""
    wdays = config.week_days

    if day == 'sunday':
        return wdays[0], 3 - week
    else:
        idx = wdays.index(day)
        week = 3 - week if day == 'saturday' else week
        return wdays[(idx + 1) % len(wdays)], week


def format_respond(sched, day=''):
    """Returns formated respond from list of named tuples 'sched'"""
    resp = ''
    if day:
        resp += '<b>{}</b>\n'.format(day.upper())
    for entry in sched:
        time, address, room, lesson, teacher = entry
        resp += '<b>{}</b>\n{}, <b>{}</b>\n'.format(time, lesson, teacher)
        resp += '<i>{}</i>, <b>{}</b>\n'.format(address, room)

    return resp
