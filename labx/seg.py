from collections import namedtuple
import math


class SegmentTree:
    def __init__(self, string):
        self.Segment = namedtuple('Segment', ['balanced', 'opened', 'closed'])
        self.string = string
        tree_depth = math.ceil(math.log(len(string), 2)) + 1
        data_size = 2 * 2 ** tree_depth
        self.data = [self.Segment(0, 0, 0) for x in range(data_size)]
        self.__build(0, 0, len(string) - 1)

    def __build(self, current, left, right):
        if left == right:
            if self.string[left] == '(':
                self.data[current] = self.Segment(0, 1, 0)
            else:
                self.data[current] = self.Segment(0, 0, 1)
        else:
            mid = (left + right) // 2
            self.__build(2 * current + 1, left, mid)
            self.__build(2 * current + 2, mid + 1, right)

            child1 = self.data[2 * current + 1]
            child2 = self.data[2 * current + 2]

            self.data[current] = self.merge(child1, child2)

    def __query(self, current, left, right, l, r):
        if l > right or r < left:
            return (-1, -1, -1)

        if l <= left and r >= right:
            return self.data[current]

        mid = (left + right) // 2
        child1 = self.__query(2 * current + 1, left, mid, l, r)
        child2 = self.__query(2 * current + 2, mid + 1, right, l, r)

        if child1 == (-1, -1, -1):
            return child2
        elif child2 == (-1, -1, -1):
            return child1
        else:
            return self.merge(child1, child2)

    def query(self, l, r):
        return self.__query(0, 0, len(self.string) - 1, l - 1, r - 1).balanced

    def merge(self, child1, child2):
        tmp = min(child1.opened, child2.closed)
        balanced = child1.balanced + child2.balanced + tmp
        opened = child1.opened + child2.opened - tmp
        closed = child1.closed + child2.closed - tmp
        return self.Segment(balanced, opened, closed)


if __name__ == "__main__":
    brackets = input()
    segment_tree = SegmentTree(brackets)
    queries = int(input())
    for i in range(queries):
        l, r = map(int, input().split())
        print(segment_tree.query(l, r))
